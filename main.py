#!/bin/python3

import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
from configparser import ConfigParser
import os
import json
import logging
import networkx as nx
import matplotlib.pyplot as plt
from datetime import datetime

CONFIG_PATH = "config/api.conf"

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

mapping_access_point = {
    "fc:ec:da:a6:15:2e":"OpenSpace",
    "fc:ec:da:a6:15:ac":"Kitchen",
    "b4:fb:e4:73:dc:d4":"Marketing",
    "24:5a:4c:1c:e4:58":"OpenSpace-4",
    "24:5a:4c:1c:e4:f0":"Kitchen-4",
    "24:5a:4c:1c:e5:48":"Meeting-4",
}


def getCredentials(PATH): 

    CONF = ConfigParser()
    CONF.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), PATH))

    UNIFI_USER = CONF.get('unifi', 'USERNAME')
    UNIFI_PASSWORD = CONF.get('unifi', 'PASSWORD')
    UNIFI_URL = CONF.get('unifi', 'URL')
    UNIFI_PORT = CONF.get('unifi', 'PORT')
    return UNIFI_USER, UNIFI_PASSWORD, UNIFI_URL, UNIFI_PORT

def connect(UNIFI_USER, UNIFI_PASSWORD, UNIFI_URL, UNIFI_PORT): 

    headers = {"Accept": "application/json","Content-Type": "application/json"}
    data = {'username': UNIFI_USER, 'password': UNIFI_PASSWORD}
    s = requests.Session()
    r = s.post(f'{UNIFI_URL}:{UNIFI_PORT}/api/login', headers = headers,  json = data , verify = False, timeout = 1)
    print(r.headers)
    cookie = r.headers["Set-Cookie"]
    # Construct cookie

    unifises = cookie.split("unifises=")[1].split(";")[0]
    csrf_token = cookie.split("csrf_token=")[1].split(";")[0]
    cookie = f"csrf_token={csrf_token}; unifises={unifises}" 
    print(cookie)
    return s, cookie 

def getRogueAccessPoint(session,cookie, UNIFI_URL, UNIFI_PORT): 

    prefix = "/proxy/network/api/s/default/"
    endpoint = "stat/rogueap"
    headers = {"Accept": "application/json","Content-Type": "application/json", "Cookie": cookie}

    payload = {'within': 24}

    return session.get(f'{UNIFI_URL}:{UNIFI_PORT}{prefix}{endpoint}', headers = headers, data=payload, verify = False, timeout = 1).content

def getRogueAccessPointFromKibanaExport(PATH): 

    f = open(PATH, "r")
    lines = f.readlines()

    # sort the lines and add it into a table
    sortedLines = []
    for line in lines:
        if "remove" in line: 
            continue
        sortedLines.append(sortKibanaLine(line))

    # sort by access_point : 
    rogueAccessPointSortedByAP = {}
    for line in sortedLines: 
        # if access_point is not in dict, insert the first row
        if line["ap_mac"] not in rogueAccessPointSortedByAP: 
            rogueAccessPointSortedByAP[line["ap_mac"]] = [{"bssid": line["bssid"], "essid":line["essid"], "rssi":line["rssi"], "occurence":1, "first_seen":line["date"], "last_seen":line["date"]}]
        else: 
            to_increment = False
            # for each rogue_access point already detected by access_point, check if it exists and increment it
            for i in range(0,len(rogueAccessPointSortedByAP[line["ap_mac"]])): 
                # if essid already exists add +1 on occurence
                if line["bssid"] == rogueAccessPointSortedByAP[line["ap_mac"]][i]["bssid"]:
                    to_increment=True
                    rogueAccessPointSortedByAP[line["ap_mac"]][i]["occurence"] = int(rogueAccessPointSortedByAP[line["ap_mac"]][i]["occurence"]) + 1
                    rogueAccessPointSortedByAP[line["ap_mac"]][i]["first_seen"] = line["date"]
                    break
         
            # Otherwise, set occurence to 1
            if not to_increment: 
                rogueAccessPointSortedByAP[line["ap_mac"]].append({"bssid": line["bssid"], "essid":line["essid"], "rssi":line["rssi"], "occurence":1, "first_seen": line["date"], "last_seen": line["date"]})

    return rogueAccessPointSortedByAP
        

def createGraphFromDict(DICT): 

    G = nx.DiGraph()
    

def sortKibanaLine(line): 

    #line = '2022-11-28T22:29:01.835+0100 I WRITE    [conn7] update ace.rogue query: { site_id: "5bbb4f4498109c17de4be5fc", ap_mac: "24:5a:4c:1c:e4:58", bssid: "6e:3a:0e:22:9e:48" } planSummary: IXSCAN { bssid: 1, site_id: 1 } update: { $set: { age: 0, band: "na", bssid: "6e:3a:0e:22:9e:48", bw: 80, center_freq: 5210, channel: 36, essid: "GUEST-ARJOBEX", freq: 5180, is_adhoc: false, is_ubnt: false, noise: -96, rssi: 14, rssi_age: 0, security: "Open", signal: -82, site_id: "5bbb4f4498109c17de4be5fc", ap_mac: "24:5a:4c:1c:e4:58", radio: "na", radio_name: "rai0", last_seen: 1669669007, report_time: 1669669007, is_rogue: false } } keysExamined:3 docsExamined:3 nMatched:1 nModified:1 keysInserted:2 keysDeleted:2 numYields:1 locks:{ Global: { acquireCount: { r: 115, w: 115 } }, Database: { acquireCount: { w: 115 } }, Collection: { acquireCount: { w: 115 } } } 105ms'

    #print(line)
    

    date = line.split("T")[0]
    hour = line.split("T")[1].split("+")[0].split('.')[0]
    site_id = line.split("site_id: \"")[1].split("\"")[0]
    ap_mac = mapping_access_point[line.split("ap_mac: \"")[1].split("\"")[0]]
    bssid = line.split("bssid: \"")[1].split("\"")[0]
    essid = line.split("essid: \"")[1].split("\"")[0]
    rssi = line.split("rssi: ")[1].split(",")[0]

    # print("------------------------")

    # print(f"Date is {date}")
    # print(f"Hour is {hour}")
    # print(f"site_id is {site_id}")
    # print(f"ap_mac is {ap_mac}")
    # print(f"bssid is {bssid}")
    # print(f"essid is {essid}")
    # print(f"rssi is {rssi}")

    finalDict = {"date":date, "hour":hour, "site_id":site_id, "ap_mac":ap_mac, "bssid":bssid, "essid":essid, "rssi":rssi}

    return finalDict

def getTheLongestTabElement(tab):

    size = 0
    for element in tab: 
        if len(element) > size:
            size = len(element)
        else: 
            continue
    return size

def getAllKeyInATab(dict, key): 

    final_tab = []
    
    for element in dict: 
        
        final_tab.append(element[key])
    
    return final_tab

def main(): 
    print("Let's check unifi rogue endpoint")
    print()

    #UNIFI_USER, UNIFI_PASSWORD, UNIFI_URL, UNIFI_PORT = getCredentials(CONFIG_PATH)
    
    #session, cookie = connect( UNIFI_USER, UNIFI_PASSWORD, UNIFI_URL, UNIFI_PORT)

    #print(getRogueAccessPoint(session,cookie,UNIFI_URL,UNIFI_PORT))

    path = "src/20230228_kibana_export.json"
    rogue_ap = getRogueAccessPointFromKibanaExport(path)

    for key in mapping_access_point.keys(): 

        print(f"Rogue access points detected by ap {mapping_access_point[key]} are :")
        print("-------------------------------------------")
        
        if mapping_access_point[key] not in rogue_ap.keys():
            print(f"No external access point have been detected by this AP\n")
            continue

        for i in rogue_ap[mapping_access_point[key]]:
            max_length = getTheLongestTabElement(getAllKeyInATab(rogue_ap[mapping_access_point[key]],"essid"))
            
            if i["essid"] == "": 
                continue
            if int(i["rssi"]) <= -1: 
                continue
            if "2022" not in i["last_seen"]: 
                continue
            
            final_print = f'{i["essid"]}' + " "*(max_length-len(i["essid"]))
            # Print results
            print(f'{final_print} ==> first_seen : {i["first_seen"]}, last_seen : {i["last_seen"]}, bssid : {i["bssid"]}, rssi : {i["rssi"]}, occurence: {i["occurence"]}')
        print("")
        
    

if __name__ == '__main__' : main()